package com.devcamp.menudrink_crud.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.menudrink_crud.model.CVoucher;
import com.devcamp.menudrink_crud.repository.IVoucherRepository;

@RestController
@CrossOrigin
@RequestMapping("/voucher")
public class CVoucherController {
    @Autowired
	IVoucherRepository pVoucherRepository;

    @GetMapping("/all")
	public ResponseEntity<List<CVoucher>> getAllVouchers() {
		try {
			List<CVoucher> pVouchers = new ArrayList<CVoucher>();
            pVoucherRepository.findAll().forEach(pVouchers::add);
			return new ResponseEntity<>(pVouchers, HttpStatus.OK);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    @GetMapping("/{id}")
	public ResponseEntity<CVoucher> getCVoucherById(@PathVariable("id") long id) {
		Optional<CVoucher> voucherData = pVoucherRepository.findById(id);
		if (voucherData.isPresent()) {
			return new ResponseEntity<>(voucherData.get(), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

    @PostMapping("/create")
	public ResponseEntity<CVoucher> createCVoucher(@Valid @RequestBody CVoucher pVouchers) {
		try {
			CVoucher voucherdata = new CVoucher();
			voucherdata.setMaVoucher(pVouchers.getMaVoucher());
			voucherdata.setPhanTramGiamGia(pVouchers.getPhanTramGiamGia());
			voucherdata.setGhiChu(pVouchers.getGhiChu());
			voucherdata.setNgayTao(new Date());
			voucherdata.setNgayCapNhat(null);
			// CVoucher _vouchers = pVoucherRepository.save(pVouchers);
			return new ResponseEntity<>(pVoucherRepository.save(voucherdata), HttpStatus.CREATED);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

    @PutMapping("/update/{id}")
	public ResponseEntity<CVoucher> updateCVoucherById(@PathVariable("id") long id,@Valid @RequestBody CVoucher pVouchers) {
		Optional<CVoucher> voucherData = pVoucherRepository.findById(id);
		if (voucherData.isPresent()) {
            CVoucher voucher = voucherData.get();
            voucher.setMaVoucher(pVouchers.getMaVoucher());
            voucher.setPhanTramGiamGia(pVouchers.getPhanTramGiamGia());
            voucher.setGhiChu(pVouchers.getGhiChu());
			voucher.setNgayCapNhat(new Date());
			return new ResponseEntity<>(pVoucherRepository.save(voucher), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

    @DeleteMapping("/delete/{id}")
	public ResponseEntity<CVoucher> deleteCVoucherById(@PathVariable("id") long id) {
		try {
			pVoucherRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
