package com.devcamp.menudrink_crud.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import com.devcamp.menudrink_crud.model.CMenu;
import com.devcamp.menudrink_crud.repository.IMenuRepository;

@CrossOrigin
@RestController
@RequestMapping("/menu")
public class CMenuController {
	@Autowired
	private IMenuRepository pMenuRepository;
	
	@GetMapping("/all")
	public ResponseEntity<List<CMenu>> getAllMenu(){
		try {
			List<CMenu> pMenuList = new ArrayList<CMenu>();
			pMenuRepository.findAll().forEach(pMenuList::add);
			return new ResponseEntity<>(pMenuList, HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@GetMapping("/{id}")
	public ResponseEntity<Object> getMenuById(@PathVariable(name = "id") long id){
		Optional<CMenu> menuFounded = pMenuRepository.findById(id);
		if(menuFounded.isPresent()){
			return new ResponseEntity<Object>(menuFounded, HttpStatus.OK);
		} 
		return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
	}

	@PostMapping("/create")
	public ResponseEntity<Object> createNewMenu(@RequestBody CMenu newMennu){
		try {
			CMenu newmenu = new CMenu(newMennu.getSize(), newMennu.getDuongKinh(), newMennu.getSuon(), newMennu.getSalad(), newMennu.getSoLuongNuocNgot(), newMennu.getDonGia());
			return new ResponseEntity<Object>(pMenuRepository.save(newmenu), HttpStatus.OK);
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@PutMapping("/update/{id}")
	public ResponseEntity<Object> updateMenuById(@PathVariable(name = "id") long id, @RequestBody CMenu newMennu){
		try {
			Optional<CMenu> menuFounded = pMenuRepository.findById(id);
			if(menuFounded.isPresent()){
				CMenu menuData = menuFounded.get();
				menuData.setDonGia(newMennu.getDonGia());
				menuData.setDuongKinh(newMennu.getDuongKinh());
				menuData.setSalad(newMennu.getSalad());
				menuData.setSize(newMennu.getSize());
				menuData.setSoLuongNuocNgot(newMennu.getSoLuongNuocNgot());
				menuData.setSuon(newMennu.getSuon());
				return new ResponseEntity<Object>(pMenuRepository.save(menuData), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@DeleteMapping("/delete/{id}")
	public ResponseEntity<Object> deleteMenuById(@PathVariable(name = "id") long id){
		try {
			Optional<CMenu> menuFounded = pMenuRepository.findById(id);
			if(menuFounded.isPresent()){
				pMenuRepository.deleteById(id);
				return new ResponseEntity<>(HttpStatus.NO_CONTENT);
			} else {
				return new ResponseEntity<>(HttpStatus.NOT_FOUND);
			}
		} catch (Exception e) {
			return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
