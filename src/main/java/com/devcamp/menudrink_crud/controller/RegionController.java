package com.devcamp.menudrink_crud.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.web.bind.annotation.*;

import com.devcamp.menudrink_crud.model.*;
import com.devcamp.menudrink_crud.repository.*;

import java.util.*;

@RestController
public class RegionController {
	@Autowired
	private RegionRepository regionRepository;
	
	@Autowired
	private CountryRepository countryRepository;
	
	@CrossOrigin
	@PostMapping("/region/create/{id}")
	public ResponseEntity<Object> createRegion(@PathVariable("id") Long id, @RequestBody CRegion cRegion) {
		try {
			Optional<CCountry> countryData = countryRepository.findById(id);
			if (countryData.isPresent()) {
			CRegion newRole = new CRegion();
			newRole.setRegionName(cRegion.getRegionName());
			newRole.setRegionCode(cRegion.getRegionCode());
			newRole.setCountry(cRegion.getCountry());
			
			CCountry _country = countryData.get();
			newRole.setCountry(_country);
			newRole.setCountryName(_country.getCountryName());
			
			CRegion savedRole = regionRepository.save(newRole);
			return new ResponseEntity<>(savedRole, HttpStatus.CREATED);
			}
		} catch (Exception e) {
			System.out.println("+++++++++++++++++++++::::: "+e.getCause().getCause().getMessage());
			return ResponseEntity.unprocessableEntity().body("Failed to Create specified Voucher: "+e.getCause().getCause().getMessage());
		}
		return new ResponseEntity<>(HttpStatus.NOT_FOUND);
	}

	@CrossOrigin
	@PutMapping("/country/{countryid}/region/update/{id}")
	public ResponseEntity<Object> updateRegion(@PathVariable("countryid") Long countryid, @PathVariable("id") Long id, @RequestBody CRegion cRegion) {
		Optional<CCountry> country = countryRepository.findById(countryid);
		if(country.isPresent()) {
		Optional<CRegion> region = regionRepository.findById(id);
			if(region.isPresent()){	
				CRegion newRegion = region.get();
				newRegion.setRegionName(cRegion.getRegionName());
				newRegion.setRegionCode(cRegion.getRegionCode());

				CCountry newCOuntry = country.get();
				newRegion.setCountry(newCOuntry);
				return new ResponseEntity<>(regionRepository.save(newRegion), HttpStatus.OK);
			} else {
				return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
			}
		} else {
			return new ResponseEntity<>(HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@PutMapping("/region/update/{id}")
	public ResponseEntity<Object> updateRegion(@PathVariable("id") Long id, @RequestBody CRegion cRegion) {
		Optional<CRegion> region = regionRepository.findById(id);
		if(region.isPresent()){	
			CRegion newRegion = region.get();
			newRegion.setRegionName(cRegion.getRegionName());
			newRegion.setRegionCode(cRegion.getRegionCode());

			return new ResponseEntity<>(regionRepository.save(newRegion), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
		}
	}

	@CrossOrigin
	@DeleteMapping("/region/delete/{id}")
	public ResponseEntity<Object> deleteRegionById(@PathVariable Long id) {
		try {
			regionRepository.deleteById(id);
			return new ResponseEntity<>(HttpStatus.NO_CONTENT);
		} catch (Exception e) {
			System.out.println(e);
			return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}

	@CrossOrigin
	@GetMapping("/region/details/{id}")
	public CRegion getRegionById(@PathVariable Long id) {
		if (regionRepository.findById(id).isPresent())
			return regionRepository.findById(id).get();
		else
			return null;
	}


	@CrossOrigin
	@GetMapping("/region/all")
	public List<CRegion> getAllRegion() {
		return regionRepository.findAll();
	}

	@CrossOrigin
	@GetMapping("/country/{countryId}/regions")
    public List < CRegion > getRegionsByCountry(@PathVariable(value = "countryId") Long countryId) {
        return regionRepository.findByCountryId(countryId);
    }
	
	@CrossOrigin
	@GetMapping("/country/{countryId}/regions/{id}")
    public Optional<CRegion> getRegionByRegionAndCountry(@PathVariable(value = "countryId") Long countryId,@PathVariable(value = "id") Long regionId) {
        return regionRepository.findByIdAndCountryId(regionId,countryId);
    }
}

