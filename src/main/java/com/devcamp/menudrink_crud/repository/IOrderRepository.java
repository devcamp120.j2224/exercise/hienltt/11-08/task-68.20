package com.devcamp.menudrink_crud.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.menudrink_crud.model.COrder;


public interface IOrderRepository extends JpaRepository<COrder, Long>{
    List<COrder> findBycUserId(Long userId);
}
